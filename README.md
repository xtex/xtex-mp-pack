# xtex's MP Pack

> Minecraft modpack focusing on multiplayer (Survival, Creative or PvP) performance and experience optimization.

[![License](https://img.shields.io/badge/license-Apache--2.0-green?style=flat-square)](https://codeberg.org/xtex/xtex-mp-pack/src/branch/main/LICENSE) ![Game Version](https://img.shields.io/modrinth/game-versions/HsMwyVxf?style=flat-square) ![Modrinth version](https://img.shields.io/modrinth/v/HsMwyVxf?style=flat-square) [![Modrinth](https://img.shields.io/badge/get--stable-modrinth-green?style=flat-square)](https://modrinth.com/modpack/xtex-mp-pack) [![exogit](https://img.shields.io/badge/get--staging-exogit-yellowgreen?style=flat-square)](https://git.exozy.me/xtex/xtex-mp-pack-staging) [![Codeberg](https://img.shields.io/badge/get--dev-codeberg-yellow?style=flat-square)](https://codeberg.org/xtex/xtex-mp-pack)

This Fabric-modpack is client-only.

Get more tips from [the wiki](https://codeberg.org/xtex/xtex-mp-pack/wiki/Home).

## Download

### Stable

The stable versions are available at [Modrinth](https://modrinth.com/modpack/xtex-mp-pack).

### Staging (recommended)

> Use **at your own risk**. Some of your customized configurations may be erased during the update.

The staging version is the delayed version of the dev. It is hosted on [exogit](https://exozy.me) and will be synchronized with dev every 24h, so some temporary bugs may be fixed in time.

[packwiz bootstrap URL](https://git.exozy.me/xtex/xtex-mp-pack-staging/raw/branch/main/pack.toml) | [MultiMC](https://cloud.exozy.me/s/2C8NamM8biP4QZz)

### Dev: packwiz installer

> Using this bootstrap URL means your client will always be in sync with the latest development branch **at your own risk**.
>
> To keep your own customized configurations, disable custom pre-launch command after first launch.

[packwiz bootstrap URL](https://codeberg.org/xtex/xtex-mp-pack/raw/branch/main/pack.toml) | [MultiMC](https://cloud.exozy.me/s/yWWfzxpSERF7roF)

## Credits

See the [CREDITS.md](https://codeberg.org/xtex/xtex-mp-pack/src/branch/main/CREDITS.md).

## License

This modpack is released under the Apache-2.0 license.

> Copyright 2023 xtex
>
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
>
> http://www.apache.org/licenses/LICENSE-2.0