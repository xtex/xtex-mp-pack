# Credits

Thanks to everyone who contributed to xtex's MP Pack and the parts it uses.

## Credit Links

- [Xaero's Minimap](https://modrinth.com/mod/xaeros-minimap) & [Xaero's World Map](https://modrinth.com/mod/xaeros-world-map)

- [XeKr Redstone Display](https://modrinth.com/resourcepack/xk-redstone-display) & [XeKr Square Pattern](https://modrinth.com/resourcepack/xekrsquarepattern)

## Direct Contributors

<!--BEGIN CONTRIBUTORS LIST-->

- xtex

<!--END CONTRIBUTORS LIST-->

## Content List

This is a complete list of all mods, resource packs and shaders included in this pack.

<!--BEGIN MOD LIST-->

- 3D Skin Layers
- AdvancementInfo Reloaded
- AppleSkin
- Architectury API
- AudioShutdown
- bad packets
- BetterF3
- Better Ping Display [Fabric]
- Better Sky
- Carpet
- Carpet Extra
- Chat Heads
- Chest Tracker
- Clickthrough 2.0
- Cloth Config API
- Concurrent Chunk Management Engine (Fabric)
- Custom Crosshair Mod
- Custom Splash Screen
- Debugify
- Default Dark Mode
- Durability Tooltip
- Dynamic FPS
- e4mc
- Entity Culling
- Fabric API
- Fabric Language Kotlin
- FastChest
- FerriteCore
- FlightHelper
- Foliage+
- ImmediatelyFast
- Indium
- Inventory HUD+
- Inventory Profiles Next
- Iris Shaders
- Key Language Pack
- Krypton
- LAN World Plug-n-Play
- lazy-language-loader
- libIPN
- Litematica
- Litematica-Printer-Easyplace-Extension
- Lithium
- Log Cleaner
- MakeUp - Ultra Fast
- MaLiLib
- MixinTrace
- ModernFix
- Mod Menu
- More Culling
- No Chat Reports
- ObsidianUI
- Pseudo-localization Language Pack
- Quit or Not
- Redstone Tweaks
- Reese's Sodium Options
- ReplayMod
- Roughly Enough Items (REI)
- RyoamicLights
- Screenshot to Clipboard
- Shiny Ores
- Sodium
- Sodium Extra
- Sofron oCd
- spark
- StackDeobfuscator
- SuperMartijn642's Config Lib
- TweakerMore
- Tweakeroo
- Twemojis In Minecraft
- Very Many Players (Fabric)
- ViaFabricPlus
- Who am I?
- WTHIT
- Xaero's Minimap
- Xaero's World Map
- XeKrSquarePattern
- YetAnotherConfigLib

<!--END MOD LIST-->

## Modrinth Links

<!--BEGIN MR LINKS LIST-->

- [Inventory Profiles Next](https://modrinth.com/mod/O7RBXm3n)
- [ModernFix](https://modrinth.com/mod/nmDcB62a)
- [WTHIT](https://modrinth.com/mod/6AQIaxuO)
- [Reese's Sodium Options](https://modrinth.com/mod/Bh37bMuy)
- [Fabric API](https://modrinth.com/mod/P7dR8mSH)
- [3D Skin Layers](https://modrinth.com/mod/zV5r3pPn)
- [TweakerMore](https://modrinth.com/mod/GBeCx05I)
- [Very Many Players (Fabric)](https://modrinth.com/mod/wnEe9KBa)
- [Xaero's Minimap](https://modrinth.com/mod/1bokaNcj)
- [Custom Splash Screen](https://modrinth.com/mod/BwFQLeCh)
- [FastChest](https://modrinth.com/mod/WNcgffMw)
- [Litematica-Printer-Easyplace-Extension](https://modrinth.com/mod/ITCqumHN)
- [AdvancementInfo Reloaded](https://modrinth.com/mod/tLuRLqpa)
- [No Chat Reports](https://modrinth.com/mod/qQyHxfxd)
- [Clickthrough 2.0](https://modrinth.com/mod/ERHOxvaH)
- [AppleSkin](https://modrinth.com/mod/EsAfCjCV)
- [Xaero's World Map](https://modrinth.com/mod/NcUtCpym)
- [Debugify](https://modrinth.com/mod/QwxR6Gcd)
- [RyoamicLights](https://modrinth.com/mod/reCfnRvJ)
- [Log Cleaner](https://modrinth.com/mod/hwRo6mwQ)
- [Iris Shaders](https://modrinth.com/mod/YL57xq9U)
- [YetAnotherConfigLib](https://modrinth.com/mod/1eAoo2KR)
- [ObsidianUI](https://modrinth.com/mod/E0L8mfJZ)
- [Carpet Extra](https://modrinth.com/mod/VX3TgwQh)
- [FerriteCore](https://modrinth.com/mod/uXXizFIs)
- [libIPN](https://modrinth.com/mod/onSQdWhM)
- [Screenshot to Clipboard](https://modrinth.com/mod/1KiJRrTg)
- [StackDeobfuscator](https://modrinth.com/mod/NusMqsjF)
- [BetterF3](https://modrinth.com/mod/8shC1gFX)
- [SuperMartijn642's Config Lib](https://modrinth.com/mod/LN9BxssP)
- [Who am I?](https://modrinth.com/mod/CcxcmoLQ)
- [Entity Culling](https://modrinth.com/mod/NNAgCjsB)
- [ReplayMod](https://modrinth.com/mod/Nv2fQJo5)
- [Sodium](https://modrinth.com/mod/AANobbMI)
- [FlightHelper](https://modrinth.com/mod/1yyNJogn)
- [ViaFabricPlus](https://modrinth.com/mod/rIC2XJV4)
- [e4mc](https://modrinth.com/mod/qANg5Jrr)
- [MaLiLib](https://modrinth.com/mod/GcWjdA9I)
- [spark](https://modrinth.com/mod/l6YH9Als)
- [ImmediatelyFast](https://modrinth.com/mod/5ZwdcRci)
- [Indium](https://modrinth.com/mod/Orvt0mRa)
- [MixinTrace](https://modrinth.com/mod/sGmHWmeL)
- [Krypton](https://modrinth.com/mod/fQEb0iXm)
- [Roughly Enough Items (REI)](https://modrinth.com/mod/nfn13YXA)
- [AudioShutdown](https://modrinth.com/mod/cSJudN5h)
- [Architectury API](https://modrinth.com/mod/lhGA9TYQ)
- [bad packets](https://modrinth.com/mod/ftdbN0KK)
- [Concurrent Chunk Management Engine (Fabric)](https://modrinth.com/mod/VSNURh3q)
- [Chat Heads](https://modrinth.com/mod/Wb5oqrBJ)
- [Chest Tracker](https://modrinth.com/mod/ni4SrKmq)
- [Cloth Config API](https://modrinth.com/mod/9s6osm5g)
- [Dynamic FPS](https://modrinth.com/mod/LQ3K71Q1)
- [Custom Crosshair Mod](https://modrinth.com/mod/o1tyE5vJ)
- [Lithium](https://modrinth.com/mod/gvQqBUqZ)
- [lazy-language-loader](https://modrinth.com/mod/Nz0RSWrF)
- [Mod Menu](https://modrinth.com/mod/mOgUt4GM)
- [More Culling](https://modrinth.com/mod/51shyZVL)
- [Fabric Language Kotlin](https://modrinth.com/mod/Ha28R6CL)
- [Sodium Extra](https://modrinth.com/mod/PtjYWJkn)
- [Durability Tooltip](https://modrinth.com/mod/smUP7V3r)
- [Better Ping Display [Fabric]](https://modrinth.com/mod/MS1ZMyR7)
- [Shiny Ores](https://modrinth.com/mod/oYrEfh82)
- [Better Sky](https://modrinth.com/mod/Zrixe2pD)
- [Foliage+](https://modrinth.com/mod/kXiPMJsD)
- [Default Dark Mode](https://modrinth.com/mod/6SLU7tS5)
- [Redstone Tweaks](https://modrinth.com/mod/RvfAlf4Z)
- [Twemojis In Minecraft](https://modrinth.com/mod/WdYcUVh8)
- [XeKrSquarePattern](https://modrinth.com/mod/rq4lDGdf)
- [Pseudo-localization Language Pack](https://modrinth.com/mod/oZcsj8Sx)
- [Key Language Pack](https://modrinth.com/mod/QCLPvYpB)
- [MakeUp - Ultra Fast](https://modrinth.com/mod/izsIPI7a)

<!--END MR LINKS LIST-->
